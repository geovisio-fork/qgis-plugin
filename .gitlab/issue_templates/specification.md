## Description

Tasks:

- [ ]

## Points of interest

<!-- Things that really matters  -->

## Log and user information

### Successes

<!-- Messages that are displayed to end-users when everything is fine  -->

### Errors

<!-- Exceptions types, errors messages  -->

## Implementation ideas

<!-- Here write some ideas that can be useful for developers -->
