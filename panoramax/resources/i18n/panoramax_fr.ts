<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>PluginPanoramaxPlugin</name>
    <message>
        <location filename="../../plugin_main.py" line="68"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="77"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="153"/>
        <source>Everything ran OK.</source>
        <translation>Tout s&apos;est bien déroulé.</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="159"/>
        <source>Houston, we&apos;ve got a problem: {err}</source>
        <translation>Houston, on a eu un problème : {err}</translation>
    </message>
</context>
<context>
    <name>wdg_panoramax_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>Panoramax - Settings</source>
        <translation>Panoramax - Préférences</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="75"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="124"/>
        <source>Report an issue</source>
        <translation>Signaler une anomalie</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="146"/>
        <source>Version used to save settings:</source>
        <translation>Version utilisée pour sauvegarder les paramètres :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="168"/>
        <source>Help</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="190"/>
        <source>Reset setttings to factory defaults</source>
        <translation>Réinitialiser les paramètres aux valeurs par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="209"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="218"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode debug activé (performances dégradées)</translation>
    </message>
</context>
</TS>
