#! python3  # noqa: E265

"""
    Authentication dialog logic.
"""


# standard
from functools import partial
from pathlib import Path
from tempfile import TemporaryDirectory

# PyQGIS
from qgis.core import Qgis, QgsApplication, QgsAuthMethodConfig
from qgis.PyQt import uic
from qgis.PyQt.Qt import QUrl
from qgis.PyQt.QtGui import QDesktopServices, QIcon
from qgis.PyQt.QtWidgets import QDialog, QLabel

# Plugin
from panoramax.__about__ import DIR_PLUGIN_ROOT, __title_clean__, __version__
from panoramax.gui.gui_commons import QVAL_URL, GuiCommonUtils
from panoramax.logic.authentification import AuthenticationTools
from panoramax.toolbelt import PlgLogger, PlgOptionsManager
from panoramax.toolbelt.network_manager import NetworkRequestsManager
from panoramax.toolbelt.slugger import sluggy


class AuthenticationDialog(QDialog):
    def __init__(self, parent=None):
        """Dialog to define current geotuileur connection as authentication config.

        :param parent: parent widget, defaults to None
        :type parent: QObject, optional
        """
        # init module and ui
        super().__init__(parent)
        uic.loadUi(Path(__file__).parent / f"{Path(__file__).stem}.ui", self)

        # toolbelt
        self.log = PlgLogger().log
        self.plg_settings_mngr = PlgOptionsManager()
        self.plg_settings = self.plg_settings_mngr.get_plg_settings()
        self.gui_utils = GuiCommonUtils()

        # icons
        self.setWindowIcon(QgsApplication.getThemeIcon("iconNewTabEditorConsole.svg"))
        self.btn_token_generate.setIcon(
            QIcon(
                str(
                    DIR_PLUGIN_ROOT.joinpath(
                        "resources/images/logo_jwt_bg_transparent.svg"
                    )
                )
            )
        )
        self.btn_token_associate.setIcon(
            QgsApplication.getThemeIcon("mActionEditInsertLink.svg")
        )
        self.btn_check_save.setIcon(
            QgsApplication.getThemeIcon("mActionFileSaveAs.svg")
        )

        # ui management
        self.btn_token_associate.setEnabled(False)
        self.lbl_instance_url.setBuddy(self.lne_instance_url)
        self.lbl_instance_name.setBuddy(self.lne_instance_name)
        self.lbl_instance_alias.setBuddy(self.lne_instance_alias)

        # init related token vars
        self.token_json_path: Path | None = None
        self.token: str | None = None

        # check inputs
        self.lne_instance_url.setValidator(QVAL_URL)

        # enhance some widgets
        self.gui_utils.make_qlabel_copiable(
            target_qlabel=self.lbl_token_claim_url,
            buddy_widget=self.lbl_help_manual_association,
        )

        # connect widgets
        self.lne_instance_name.textChanged.connect(self.on_instance_name_change)
        self.btn_token_generate.clicked.connect(self.generate_token)
        self.btn_check_save.clicked.connect(self.check_and_save_instance)

        # Authentication
        self.authent_tools = AuthenticationTools()

        # NetworkRequestsManager
        self.network_request_manager = NetworkRequestsManager()

    def check_required_fields(self) -> bool:
        """Check if form/dialog required fields are not correctly filled.

        :return: True if all required fields have a valid value. False if not.
        :rtype: bool
        """
        # check required fields
        required_fields_buddies: tuple[QLabel] = (
            self.lbl_instance_url,
            self.lbl_instance_name,
            self.lbl_instance_alias,
        )
        invalid_fields = []
        for field in required_fields_buddies:
            if not field.buddy().text() or field.buddy().text() == "":
                invalid_fields.append(field.text().rsplit(":")[0].strip())

        if count_invalid := len(invalid_fields):
            self.log(
                message=self.tr(
                    "{} required fields are empty: {}".format(
                        count_invalid, ", ".join(invalid_fields)
                    )
                ),
                log_level=2,
                push=True,
                duration=20,
            )
            return False

        return True

    def check_and_save_instance(self) -> bool:
        """Check if connection is valid for a qgis authentication id.
        Display a message box with user name and last name if connection valid, or
        message with error message otherwise.

        :return: True if connection is valid for qgis_auth_id, False otherwise
        :rtype: bool
        """
        if not self.check_required_fields():
            return

        # check token validity
        self.authent_tools.check_jwt(in_jwt_token=self.authent_tools.jwt_token)

        self.log(
            message=self.tr("Check connection"),
            log_level=3,
            push=True,
            duration=5,
            parent_location=self,
        )
        self.add_auth_manager()

    def on_instance_name_change(self):
        """Actions performed when the text of instance name field changed."""
        if instance_name := self.lne_instance_name.text():
            self.lne_instance_alias.setText(sluggy(instance_name))
        else:
            self.lne_instance_alias.clear()

    def generate_token(self):
        """Create a temporary folder and generate the api route to download the json
        token into the temporary folder. Finally, the file token
        verification method is launched.
        """
        # build POST API URL
        api_url = self.authent_tools.build_api_generate_token_url(
            url_instance=self.lne_instance_url.text(),
            description=f"QGIS_plugin_{sluggy(__version__)}",
        )

        # download token
        with TemporaryDirectory(
            prefix=__title_clean__, ignore_cleanup_errors=True
        ) as tmpdir:
            token_json_path = Path(tmpdir).joinpath(
                f"{self.lne_instance_alias.text()}_token.json"
            )

            # Download the token file to json_path
            self.network_request_manager.download_file(
                remote_url=api_url,
                local_path=token_json_path,
                method=Qgis.HttpMethod.Post,
            )

            self.authent_tools.load_generated_token(token_json_path)

        self.check_if_token_exist()

    def associate_token(self):
        """Set claim URL to label and button."""

        # enable associate button
        self.btn_token_associate.setEnabled(True)

        # clear and fill the manual line edit to allow end-user copy paste
        self.lbl_token_claim_url.clear()
        self.lbl_token_claim_url.setText(self.authent_tools.claim_url)

        # connect association button
        self.btn_token_associate.clicked.connect(
            partial(
                QDesktopServices.openUrl,
                QUrl(self.authent_tools.claim_url),
            )
        )

    def check_if_token_exist(self):
        """Check that the token json has been downloaded; if so, load it and perfom
        next actions. If not, an error message is returned.
        """
        if (
            self.authent_tools._generated_token is not None
            and self.authent_tools.check_jwt()
        ):
            self.associate_token()
        else:
            PlgLogger.log(
                "Error retrieving token, please check instance URL or network logs (F12)",
                log_level=2,
                push=True,
                duration=30,
                parent_location=self,
            )

    def add_auth_manager(self):
        """Adds an entry to the qgis authentication manager. Create an API Header
        authentication with name, token and url.
        """
        new_auth = QgsAuthMethodConfig()
        new_auth.setId(QgsApplication.authManager().uniqueConfigId())
        new_auth.setName(self.lne_instance_name.text())
        new_auth.setUri(self.lne_instance_url.text())
        new_auth.setConfig("jwt_token", self.authent_tools.jwt_token)
        new_auth.setMethod("APIHeader")
        auth_manager = QgsApplication.authManager()
        auth_manager.storeAuthenticationConfig(new_auth, True)
