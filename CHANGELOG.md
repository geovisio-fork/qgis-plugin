# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 0.2.0 - 2023-08-03

- Generate an alias sluguifying the instance name
- URL Validator - RFC compliant
- Button and request to check token validity
- Button and logic (web browser opening) to associate the token with user account
- Request to download token (using QgsFileDownloader)
- Addition of a form UI (entry of the instance url and button for flow authentication)

## 0.1.0 - 2023-07-28

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
